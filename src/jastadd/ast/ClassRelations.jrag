/* Copyright (c) 2005-2013, The JastAdd Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
import org.jastadd.ast.AST.*;
import java.util.*;

aspect Superclass {
  rewrite ASTDecl {
    when(!hasSuperClass() && !isASTNodeDecl())
    to ASTDecl {
      setSuperClass(new IdUse(config().astNodeType()));
      return this;
    }
  }

  syn boolean ASTDecl.testCircular(String name) {
    if (!hasSuperClass())
      return false;
    if (getSuperClassName().equals(name))
      return true;
    ASTDecl superClass = (ASTDecl) grammar().lookup(getSuperClassName());
    return superClass != null ? superClass.testCircular(name) : false;
  }

  syn boolean ASTDecl.isCircular() = testCircular(name());

  syn lazy ASTDecl ASTDecl.superClass() = hasSuperClass() && !isCircular() ?
    (ASTDecl) grammar().lookup(getSuperClassName()) : null;

  syn lazy String ASTDecl.getSuperClassName() = hasSuperClass() ?
    getSuperClass().name() : null;

  /**
   * @return all supertypes of this AST type
   */
  syn lazy Collection<ASTDecl> ASTDecl.supertypes() {
    if (superClass() != null) {
      Collection<ASTDecl> types = new LinkedList<ASTDecl>();
      types.addAll(superClass().supertypes());
      types.add(superClass());
      return types;
    } else {
      return Collections.emptyList();
    }
  }
}

aspect InstanceOf {
  syn boolean TypeDecl.instanceOf(TypeDecl c) = c == this;

  eq ASTDecl.instanceOf(TypeDecl c) {
    if (c == this) {
      return true;
    }
    TypeDecl superClass = superClass();
    return superClass != null ? superClass.instanceOf(c) : false;
  }
}

aspect Subclasses {
  syn lazy Map<ASTDecl,Collection<ASTDecl>> Grammar.subclassMap() {
    Map<ASTDecl,Collection<ASTDecl>> map = new HashMap<ASTDecl,Collection<ASTDecl>>();
    for (int j = 0; j < getNumTypeDecl(); j++) {
      if (getTypeDecl(j) instanceof ASTDecl) {
        ASTDecl decl = (ASTDecl) getTypeDecl(j);
        map.put(decl, new ArrayList<ASTDecl>());
      }
    }
    for (int j = 0; j < getNumTypeDecl(); j++) {
      if (getTypeDecl(j) instanceof ASTDecl) {
        ASTDecl decl = (ASTDecl) getTypeDecl(j);
        if (decl.superClass() != null) {
          map.get(decl.superClass()).add(decl);
        }
      }
    }
    return map;
  }

  inh lazy Collection<ASTDecl> TypeDecl.findSubclasses(ASTDecl target);
  syn lazy Collection<ASTDecl> ASTDecl.subclasses() = findSubclasses(this);

  eq Grammar.getTypeDecl().findSubclasses(ASTDecl target) {
    return subclassMap().get(target);
  }

  syn lazy Set<ASTDecl> ASTDecl.subclassesTransitive() {
    Set<ASTDecl> set = new LinkedHashSet<ASTDecl>(subclasses());
    for (Iterator it = subclasses().iterator(); it.hasNext(); ) {
      ASTDecl decl = (ASTDecl) it.next();
      set.addAll(decl.subclassesTransitive());
    }
    return set;
  }
}

aspect Parents {
  syn lazy HashMap Grammar.parentMap() {
    HashMap map = new LinkedHashMap();
    for (int j = 0; j < getNumTypeDecl(); j++) {
      if (getTypeDecl(j) instanceof ASTDecl) {
        ASTDecl decl = (ASTDecl)getTypeDecl(j);
        map.put(decl, new LinkedHashSet());
      }
    }

    for (int j = 0; j < getNumTypeDecl(); j++) {
      if (getTypeDecl(j) instanceof ASTDecl) {
        ASTDecl decl = (ASTDecl)getTypeDecl(j);
        for (Iterator iter = decl.getComponents(); iter.hasNext(); ) {
          Components c = (Components)iter.next();
          if (!(c instanceof TokenComponent)) {
            TypeDecl t = lookup(c.type());
            if (t != null) {
              ((HashSet)map.get(t)).add(decl);
            }
          }
        }
      }
    }
    return map;
  }

  eq Grammar.getTypeDecl().findParents(ASTDecl node) {
    Set<ASTDecl> set = new LinkedHashSet<ASTDecl>();
    set.addAll((Collection) parentMap().get(node));
    if (node.superClass() != null)
      set.addAll(node.superClass().parents());
    return set;
  }

  inh Collection<ASTDecl> TypeDecl.findParents(ASTDecl node);
  syn lazy Collection<ASTDecl> ASTDecl.parents() = findParents(this);

  // Do not include parents of super classes (as parents() does)
  inh lazy Collection<ASTDecl> TypeDecl.parentsIntransitive();
  eq Grammar.getTypeDecl(int i).parentsIntransitive()
    = new LinkedHashSet<ASTDecl>((Collection) parentMap().get(getTypeDecl(i)));
}
