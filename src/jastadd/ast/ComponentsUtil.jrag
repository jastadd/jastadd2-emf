/* Copyright (c) 2005-2013, The JastAdd Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
import org.jastadd.ast.AST.*;
import java.util.*;

aspect ASTDecl {
  syn lazy Collection<Components> ASTDecl.components() {
    LinkedList<Components> list = new LinkedList<Components>();
    if (superClass() != null) {
      list.addAll(superClass().components());
    }
    for (int i = 0; i < getNumComponents(); i++) {
      boolean done = false;
      for (ListIterator<Components> iter = list.listIterator(); !done && iter.hasNext(); ) {
        Components c = iter.next();
        if (c.name().equals(getComponents(i).name()) && c.type().equals(getComponents(i).type())) {
          iter.remove();
          done = true;
        }
      }
      if (getComponents(i).isNTA()) {
        list.add(getComponents(i));
      }
      else {
        int j = 0;
        while (j < list.size() && !((Components)list.get(j)).isNTA())
          j++;
        list.add(j, getComponents(i));
      }
    }
    return list;
  }

  syn Iterator<Components> ASTDecl.getComponents() =
    components().iterator();

  syn boolean ASTDecl.redefinesTokenComponent(TokenComponent c) {
    if (c.hostClass() != this) // inherited component
      return false;
    if (superClass() == null) // no definition in superclass
      return true;
    for (Iterator iter = superClass().getComponents(); iter.hasNext(); ) {
      Components d = (Components)iter.next();
      if (d.name().equals(c.name()) && d instanceof TokenComponent && c.isNTA() == d.isNTA()) {
        return false;
      }
    }
    return true; // no equal definition in superclass
  }
}
aspect Comp {
  public Iterator<Components> TypeDecl.getComponents() {
    return components().iterator();
  }
  syn lazy Collection<Components> TypeDecl.components() {
    LinkedList list = new LinkedList();
    for (int i = 0; i < getNumComponents(); i++) {
      list.add(getComponents(i));
    }
    return list;
  }

  /**
   * @return {@code true} if this attribute is an NTA shadowing a non-NTA child
   * of a supertype
   */
  syn boolean Components.ntaShadowingNonNTA() {
    if (!isNTA()) {
      return false;
    }
    String name = name();
    TypeDecl hostClass = hostClass();
    while (true) {
      hostClass = hostClass instanceof ASTDecl ? ((ASTDecl) hostClass).superClass() : null;
      if (hostClass == null) {
        return false;
      }
      for (Iterator iter = hostClass.getComponents(); iter.hasNext(); ) {
        Components comp = (Components) iter.next();
        if (!comp.isNTA() && name.equals(comp.name())) {
          return true;
        }
      }
    }
  }

}

aspect NTADetector {
  public boolean Components.isNTA() {
    return false;
  }
  public boolean ListComponentsNTA.isNTA() {
    return true;
  }
  public boolean OptionalComponentNTA.isNTA() {
    return true;
  }
  public boolean TokenComponentNTA.isNTA() {
    return true;
  }
  public boolean AggregateComponentsNTA.isNTA() {
    return true;
  }
}

aspect ConstructorParameterTypes {
  syn String Components.constrParmType();
  eq ListComponents.constrParmType() = config().listType() + "<" + getId().type() + ">";
  eq OptionalComponent.constrParmType() = config().optType() + "<" + getId().type() + ">";
  eq TokenComponent.constrParmType() = getTokenId().getTYPE();
  eq AggregateComponents.constrParmType() = getId().type();
}

aspect ComponentsHostClass {
  inh TypeDecl Components.hostClass();

  public String ClassDecl.extendsName = "java.lang.Object";
}


// ES_2011-12-05: Checks for regions in coarse incremental evaluation
aspect CoarseIncremental {

  syn lazy boolean ASTDecl.isRegionRoot() = isRootNode() || lookupRegionDecl(name()) != null;

  inh RegionDecl ASTDecl.lookupRegionDecl(String name);
  eq Grammar.getTypeDecl().lookupRegionDecl(String name) {
    for (int i = 0; i < getNumRegionDecl(); i++) {
      RegionDecl decl = getRegionDecl(i);
      if (decl.name().equals(name)) {
        return decl;
      }
    }
    return null;
  }

  syn lazy boolean ASTDecl.hasRegionRootAsSuperClass() {
    ASTDecl superDecl = superClass();
    if (superDecl != null) {
      if (superDecl.isRegionRoot()) {
        return true;
      }
      return superDecl.hasRegionRootAsSuperClass();
    }
    return false;
  }

  syn lazy boolean ASTDecl.isRegionLeaf() {

    if (name().equals(config().listType()) || name().equals(config().optType())) {
      TypeDecl type = grammar().lookup(config().astNodeType());
      if (type != null && type instanceof ASTDecl && ((ASTDecl)type).isRegionRoot()) {
          return false;
      }
      return true;
    }

    for (int i = 0; i < getNumComponents(); i++) {
      Components comp = getComponents(i);
      TypeDecl type = grammar().lookup(comp.type());
      if (type != null && type instanceof ASTDecl && ((ASTDecl)type).isRegionRoot()) {
        return true;
      }
    }
    return false;
  }


}
//
