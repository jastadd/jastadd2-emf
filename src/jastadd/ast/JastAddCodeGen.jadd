/* Copyright (c) 2005-2013, The JastAdd Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import org.jastadd.ast.AST.*;

import java.io.*;

import java.util.*;

import org.jastadd.jrag.AST.ASTAspectMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectRefineMethodDeclaration;
import org.jastadd.jrag.AST.ASTAspectFieldDeclaration;
import org.jastadd.jrag.AST.ASTBlock;
import org.jastadd.jrag.ClassBodyObject;

aspect JastAddCodeGen {
  public void Grammar.weaveInterfaceIntroductions() {
    for (int i = 0; i < getNumTypeDecl(); i++) {
      if (getTypeDecl(i) instanceof InterfaceDecl) {
        InterfaceDecl d = (InterfaceDecl)getTypeDecl(i);
        String name = d.name();
        //System.out.println("Processing interface " + name);

        for (int j = 0; j < getNumTypeDecl(); j++) {
          if (getTypeDecl(j) instanceof ASTDecl) {
            ASTDecl dest = (ASTDecl)getTypeDecl(j);
            if (dest.implementsInterface(name)) {
              //System.out.println("  implemented by " + dest.name());
              for (Iterator iter = d.getClassBodyDeclsItr(); iter.hasNext(); ) {
                ClassBodyObject o = (ClassBodyObject)iter.next();
                if (o.node instanceof ASTAspectMethodDeclaration || o.node instanceof ASTAspectFieldDeclaration) {
                  if (!dest.hasClassBodyDecl(o.signature()))
                    dest.classBodyDecls.add(new ClassBodyObject(o.node/*.fullCopy()*/, o.fileName, o.line, o.getAspectName()));
                }
                else if (o.node instanceof ASTAspectRefineMethodDeclaration) {
                  ClassBodyObject object = new ClassBodyObject(o.node/*.fullCopy()*/, o.fileName, o.line, o.getAspectName());
                  object.refinesAspect = o.refinesAspect;
                  dest.classBodyDecls.add(object);
                }
                else if (o.node instanceof ASTBlock) {
                  dest.classBodyDecls.add(new ClassBodyObject(o.node/*.fullCopy()*/, o.fileName, o.line, o.getAspectName()));
                }
              }
              for (Iterator iter = d.refinedClassBodyDecls.iterator(); iter.hasNext(); ) {
                  ClassBodyObject o = (ClassBodyObject)iter.next();
                  if (o.node instanceof ASTAspectMethodDeclaration || o.node instanceof ASTAspectFieldDeclaration) {
                    if (!dest.hasClassBodyDecl(o.signature()))
                      dest.refinedClassBodyDecls.add(new ClassBodyObject(o.node/*.fullCopy()*/, o.fileName, o.line, o.getAspectName()));
                  }
                  else if (o.node instanceof ASTAspectRefineMethodDeclaration) {
                    ClassBodyObject object = new ClassBodyObject(o.node/*.fullCopy()*/, o.fileName, o.line, o.getAspectName());
                    object.refinesAspect = o.refinesAspect;
                    dest.refinedClassBodyDecls.add(object);
                  }
                  else if (o.node instanceof ASTBlock) {
                    dest.classBodyDecls.add(new ClassBodyObject(o.node/*.fullCopy()*/, o.fileName, o.line, o.getAspectName()));
                  }
              }

              for (int k = 0; k < d.getNumSynDecl(); k++) {
                //System.out.println("    adding syndecl " + d.getSynDecl(k).signature());
                dest.addSynDecl((SynDecl)d.getSynDecl(k).fullCopy());
              }

              for (int k = 0; k < d.getNumSynEq(); k++) {
                //System.out.println("    adding syneq " + d.getSynEq(k).signature());
                dest.addSynEq((SynEq)d.getSynEq(k).fullCopy());
              }

              for (int k = 0; k < d.getNumInhDecl(); k++) {
                //System.out.println("    adding inhdecl " + d.getInhDecl(k).signature());
                dest.addInhDecl((InhDecl)d.getInhDecl(k).fullCopy());
              }

              for (int k = 0; k < d.getNumInhEq(); k++) {
                //System.out.println("    adding inheq " + d.getInhEq(k).signature());
                dest.addInhEq((InhEq)d.getInhEq(k).fullCopy());
              }
            }
          }
        }
      }
    }
  }

  public boolean ASTDecl.hasClassBodyDecl(String signature) {
    for (Iterator iter = getClassBodyDeclsItr(); iter.hasNext(); ) {
      ClassBodyObject o = (ClassBodyObject)iter.next();
      if (o.signature().equals(signature))
        return true;
    }
    return false;
  }

  public void Grammar.jastAddGen(boolean publicModifier) {
    createPackageOutputDirectory();

    for (int i = 0; i < getNumTypeDecl(); i++) {
      getTypeDecl(i).jastAddGen(publicModifier);
    }
  }

  public String TypeDecl.modifiers = "";

  syn String TypeDecl.modifiers() {
    if (modifiers == null) {
      return "";
    } else {
      return modifiers + " ";
    }
  }

  public String TypeDecl.typeParameters = "";

  public String TypeDecl.typeDeclarationString() {
    return "";
  }

  public String ClassDecl.typeDeclarationString() {
    String s = interfacesString();
    if (s.equals(""))
      return modifiers() + "class " + getIdDecl().getID() + typeParameters + " extends " + extendsName + " {";
    else
      return modifiers() + "class " + getIdDecl().getID() + typeParameters + " extends " + extendsName + " implements " +
        s + " {";
  }

  public String InterfaceDecl.typeDeclarationString() {
    String s = interfacesString();
    if (s.equals(""))
      return modifiers() + "interface " + getIdDecl().getID() + typeParameters + " {";
    else
      return modifiers() + "interface " + getIdDecl().getID() + typeParameters + " extends " + s + " {";
  }

  public String EnumDecl.typeDeclarationString() {
    String s = interfacesString();
    if (s.isEmpty())
      return modifiers() + "enum " + getIdDecl().getID() + typeParameters + " {";
    else
      return modifiers() + "enum " + getIdDecl().getID() + typeParameters + " implements " + s + " {";
  }

  public String ASTDecl.componentString() {
    StringBuffer buf = new StringBuffer();
    for (int i = 0; i < getNumComponents(); ++i) {
      buf.append(" ");
      buf.append("<span class=\"component\">");
      buf.append(getComponents(i).componentString());
      buf.append("</span>");
    }
    return buf.toString();
  }
  public String Id.componentString() {
    String id = "{@link "+getIdUse().getID()+"}";
    if (hasNameNode())
      return getNameNode().getID()+":"+id;
    return id;
  }
  public String TokenId.componentString() {
    return getID()+":"+getTYPE();
  }
  public abstract String Components.componentString();
  public String ListComponents.componentString() {
    return getId().componentString()+"*";
  }
  public String OptionalComponent.componentString() {
    return "["+getId().componentString()+"]";
  }
  public String TokenComponent.componentString() {
    return "&lt;"+getTokenId().componentString()+"&gt;";
  }
  public String AggregateComponents.componentString() {
    return getId().componentString();
  }

  public void TypeDecl.jastAddGen(boolean publicModifier) {
  }

  public void InterfaceDecl.jastAddGen(boolean publicModifier) {
    File file = grammar().targetJavaFile(name());
    try {
      PrintStream stream = new PrintStream(new FileOutputStream(file));
      if ( !config().license.isEmpty()) {
        stream.println(config().license);
      }

      // TODO move to template
      if (!config().packageName().isEmpty()) {
        stream.println("package " + config().packageName() + ";\n");
      }

      stream.print(grammar().genImportsList());
      stream.println(docComment());
      stream.println(typeDeclarationString());

      /*
      StringBuffer buf = new StringBuffer();
      for (Iterator iter = getClassBodyDeclsItr(); iter.hasNext(); ) {
        ClassBodyObject o = (ClassBodyObject)iter.next();
        jrag.AST.SimpleNode n = o.node;

        if (!(n instanceof ASTAspectMethodDeclaration) && !(n instanceof ASTAspectFieldDeclaration)) {
          buf.append("    // Declared in " + o.fileName + " at line " + o.line + "\n");
          n.jjtAccept(new ClassBodyDeclUnparser(), buf);
          buf.append("\n\n");
        }
      }
      stream.println(buf.toString());
      */

      emitMembers(stream);
      emitAbstractSyns(stream);
      //emitSynEquations(stream);
      emitInhDeclarations(stream);
      //emitInhEquations(stream);
      emitInterfaceCollections(stream);

      stream.println("}");
      stream.close();
    } catch (FileNotFoundException f) {
      System.err.println("Could not create file " + file.getName() + " in " + file.getParent());
      System.exit(1);
    }
  }

  public org.jastadd.jrag.AST.SimpleNode EnumDecl.simpleNode = null;
  public void EnumDecl.jastAddGen(boolean publicModifier) {
    File file = grammar().targetJavaFile(name());
    try {
      PrintStream stream = new PrintStream(new FileOutputStream(file));
      if (!config().license.isEmpty()) {
        stream.println(config().license);
      }

      // TODO move to template
      if (!config().packageName().isEmpty()) {
        stream.println("package " + config().packageName() + ";\n");
      }

      stream.print(grammar().genImportsList());
      stream.println(docComment());

      stream.println(Unparser.unparse(simpleNode));

      stream.close();
    } catch (FileNotFoundException f) {
      System.err.println("Could not create file " + file.getName() + " in " + file.getParent());
      System.exit(1);
    }
  }

  public void ClassDecl.jastAddGen(boolean publicModifier) {
    File file = grammar().targetJavaFile(name());
    try {
      PrintStream stream = new PrintStream(new FileOutputStream(file));
      if ( !config().license.isEmpty()) {
        stream.println(config().license);
      }

      // TODO move to template
      if (!config().packageName().isEmpty()) {
        stream.println("package " + config().packageName() + ";\n");
      }

      stream.print(grammar().genImportsList());
      stream.println(docComment());
      stream.println(typeDeclarationString());
      StringBuffer buf = new StringBuffer();
      for (Iterator iter = getClassBodyDeclsItr(); iter.hasNext(); ) {
        ClassBodyObject obj = (ClassBodyObject)iter.next();
        org.jastadd.jrag.AST.SimpleNode n = obj.node;

        //buf.append("    // Declared in " + obj.fileName + " at line " + obj.line + "\n");
        //if (!obj.comments.equals(""))
          //buf.append(obj.comments + " ");
        buf.append(config().indent);
        buf.append(obj.modifiers());
        n.jjtAccept(new ClassBodyDeclUnparser(), buf);
        buf.append("\n\n");
      }
      stream.println(buf.toString());

      //emitMembers(stream);
      emitAbstractSyns(stream);
      //emitSynEquations(stream);
      emitInhDeclarations(stream);
      //emitInhEquations(stream);

      stream.println("}");
      stream.close();
    } catch (FileNotFoundException f) {
      System.err.println("Could not create file " + file.getName() + " in " + file.getParent());
      System.exit(1);
    }
  }


  public void ASTDecl.jastAddGen(boolean publicModifier) {
    File file = grammar().targetJavaFile(name());
    try {
      PrintStream stream = new PrintStream(new FileOutputStream(file));

      // Insert comment notifying that this is a generated file
      stream.println("/* This file was generated with " +
          org.jastadd.JastAdd.getLongVersionString() + " */");

      if ( !config().license.isEmpty()) {
        stream.println(config().license);
      }

      // TODO move to template
      if (!config().packageName().isEmpty()) {
        stream.println("package " + config().packageName() + ";\n");
      }

      stream.print(grammar().genImportsList());

      stream.println(docComment());
      stream.print("public ");
      if (hasAbstract()) {
        stream.print("abstract ");
      }
      stream.print("class " + name());
      if (isOptDecl() || isListDecl() || isASTNodeDecl()) {
        stream.print("<T extends " + config().astNodeType() + ">");
      }
      if (hasSuperClass()) {
        stream.print(" extends ");
        String name = getSuperClass().name();
        if (isListDecl() || isOptDecl()) {
          name = name + "<T>";
        } else if (name.equals(config().astNodeType())) {
          name = name + "<" + config().astNodeType() + ">";
        }
        stream.print(name);
      } else if (isASTNodeDecl()) {
        String superType = config().astNodeSuperType();
        if (!superType.equals("")) {
          stream.print(" extends " + superType);
        }
      }
      stream.print(jastAddImplementsList());
      stream.println(" {");

      jastAddAttributes(stream);
      stream.println("}");
      stream.close();
    } catch (FileNotFoundException f) {
      System.err.println("Could not create file " + file.getName() + " in " + file.getParent());
      System.exit(1);
    }
  }

  public String ASTDecl.jastAddImplementsList() {
    StringBuffer buf = new StringBuffer();
    buf.append(" implements Cloneable");

    for (Iterator iter = implementsList.iterator(); iter.hasNext(); ) {
      buf.append(", " + Unparser.unparse((org.jastadd.jrag.AST.SimpleNode)iter.next()));
    }

    if (isASTNodeDecl()) {
      buf.append(", Iterable<T>");
    }
    return buf.toString();
  }

  public String TypeDecl.interfacesString() {
    StringBuffer buf = new StringBuffer();
    Iterator iter = implementsList.iterator();
    if (iter.hasNext()) {
      buf.append(Unparser.unparse((org.jastadd.jrag.AST.SimpleNode)iter.next()));
      while (iter.hasNext()) {
        buf.append(", " + Unparser.unparse((org.jastadd.jrag.AST.SimpleNode)iter.next()));
      }
    }
    return buf.toString();
  }

  public void ASTDecl.jastAddAttributes(PrintStream out) {
    emitMembers(out);
    emitAbstractSyns(out);
    emitSynEquations(out);
    emitInhDeclarations(out);
    emitInhEquations(out);
    if (config().rewriteEnabled()) {
      emitRewrites(out);
    }
    emitCollDecls(out);
    emitCollContributions(out);
    emitInhEqSignatures(out);
  }

}

