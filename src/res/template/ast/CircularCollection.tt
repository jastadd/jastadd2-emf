# Copyright (c) 2013, The JastAdd Team
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Lund University nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# circular collection evaluation
AttrDecl.circularCollectionEval = [[
$if(#isParameterized)
  /**
   * @apilevel internal
   */
  protected $TypeDefaultSet #(signature)_computed = $CreateDefaultSet;

  /**
   * @apilevel internal
   */
  protected $TypeDefaultSet #(signature)_initialized = $CreateDefaultSet;

  /**
   * @apilevel internal
   */
  protected $TypeDefaultMap #(signature)_values = $CreateDefaultMap;

#docComment
  #annotations
  @ASTNodeAnnotation.Attribute
  public #getType #name(#parametersDecl) {
    $SynchBegin
    #parameterStructure
    #initLazyMaps
    $include(AttrDecl.cacheCheck)
    #addComponentCheck
    if (!#(signature)_initialized.contains(_parameters)) {
      #(signature)_initialized.add(_parameters);
      #(signature)_values.put(_parameters, $RefType1);
    }
    if (!state().IN_CIRCLE) {
      state().IN_CIRCLE = true;
      #cacheInitRewrite
      #addAddToComponent
      state().CIRCLE_INDEX = 1;
      #getType new_#(signature)_value;
      do {
        #(signature)_visited.put(_parameters, new Integer(state().CIRCLE_INDEX));
        state().CHANGE = false;
        new_#(signature)_value = $BottomValue;
        combine_#(signature)_contributions(new_#(signature)_value);
        if ($Cond2) {
          state().CHANGE = true;
        }
        #(signature)_values.put(_parameters, $RefType2);
        state().CIRCLE_INDEX++;
        #cycleLimitCheck
      } while (state().CHANGE);

      if (#cacheStoreCondition) {
        #(signature)_computed.add(_parameters);
        $include(cacheCycleInit)
      } else {
        $include(resetCycleInit)
        #(signature)_computed.remove(_parameters);
        #(signature)_initialized.remove(_parameters);
      }
      state().IN_CIRCLE = false;
      return new_#(signature)_value;
    }
    if(!new Interger(state().CIRCLE_INDEX).equals(#(signature)_visited.get(_parameters))) {
      #(signature)_visited.put(_paramters, new Integer(state().CIRCLE_INDEX));
      #cacheCycleCheck
      $include(AttrDecl.resetCycleCheck)
      #getType new_#(signature)_value = $BottomValue;
      combine_#(signature)_contributions(new_#(signature)_value);
      if ($Cond2) {
        state().CHANGE = true;
      }
      #(signature)_values.put(_parameters, $RefType2);
      return new_#(signature)_value;
    }
    return $FromRefType;
    $SynchEnd
  }
$else
  /**
   * @apilevel internal
   */
  protected boolean #(signature)_computed = false;

  /**
   * @apilevel internal
   */
  protected boolean #(signature)_initialized = false;

  /**
   * @apilevel internal
   */
  protected #getType #(signature)_value;

$if(SeparateEvaluation)
  protected #getType new_#(signature)_value;
$endif

#docComment
  #annotations
  @ASTNodeAnnotation.Attribute
  public #getType #name(#parametersDecl) {
    $SynchBegin
    #parameterStructure
    $include(AttrDecl.cacheCheck)
    $ASTNode node = this;
    while(node.getParent() != null && !(node instanceof $RootType))
      node = node.getParent();
    #collDebugString
    $RootType root = ($RootType) node;

$if(!SeparateEvaluation)
    if(root.collecting_contributors_#collectingSignature)
      throw new RuntimeException("Circularity during phase 1");
$endif
    #addComponentCheck
$if(!SeparateEvaluation)
    root.collect_contributors_#collectingSignature();
$endif

    if (!#(signature)_initialized) {
      #(signature)_initialized = true;
      #(signature)_value = $BottomValue;
    }

    if (!state().IN_CIRCLE) {
      state().IN_CIRCLE = true;
      #addAddToComponent
      #cacheInitRewrite
      state().CIRCLE_INDEX = 1;
      do {
        #(signature)_visited = state().CIRCLE_INDEX;
        state().CHANGE = false;

$if(SeparateEvaluation)
        new_#(signature)_value = $BottomValue;
        root.#(name)_#(signature)_nextIteration(this);
$else
        #getType new_#(signature)_value = $BottomValue;
        combine_#(signature)_contributions(new_#(signature)_value);
$endif
        if ($Cond1) {
          state().CHANGE = true;
        }
        #(signature)_value = new_#(signature)_value;
        state().CIRCLE_INDEX++;
        #cycleLimitCheck
      } while (state().CHANGE);

      if (#cacheStoreCondition) {
        #(signature)_computed = true;
        $include(cacheCycleInit)
      } else {
        $include(resetCycleInit)
        #(signature)_computed = false;
        #(signature)_initialized = false;
      }
      state().IN_CIRCLE = false;
      $include(AttrDecl.returnStmt)
    }
    if(#(signature)_visited != state().CIRCLE_INDEX) {
      #(signature)_visited = state().CIRCLE_INDEX;
      #cacheCycleCheck
      $include(AttrDecl.resetCycleCheck)
      #addAddToComponent
$if(SeparateEvaluation)
      new_#(signature)_value = $BottomValue;
      root.#(name)_#(signature)_nextIteration(this);
$else
      #(getType) new_#(signature)_value = $BottomValue;
      combine_#(signature)_contributions(new_#(signature)_value);
$endif
      if ($Cond1) {
        state().CHANGE = true;
      }
      #(signature)_value = new_#(signature)_value;
      $include(AttrDecl.returnStmt)
    }
    $include(AttrDecl.returnStmt)
    $SynchEnd
  }
$endif
]]
