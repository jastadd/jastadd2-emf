# Copyright (c) 2013, The JastAdd Team
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Lund University nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ASTDecl.emitRewrites.begin = [[
  /**
   * @apilevel internal
   */
  public $ASTNode rewriteTo() {
]]

ASTDecl.emitRewrites.touch_list = [[
    if(list$$touched) {
      for(int i = 0 ; i < getNumChildNoTransform(); i++) {
        getChild(i);
      }
      list$$touched = false;
$if(RewriteCircularNTA)
      if (state().CHANGE) {
        return this;
      }
$else
      return this;
$endif
    }
]]

ASTDecl.emitRewrites.end:ASTNode = [[
    if(state().peek() == $ASTNode$$State.REWRITE_CHANGE) {
      state().pop();
      state().push($ASTNode$$State.REWRITE_NOCHANGE);
    }
    return this;
  }
]]

ASTDecl.emitRewrites.end:!unconditional = [[
    return super.rewriteTo();
  }
]]

ASTDecl.emitRewrites.end = [[
  }
]]

Rewrite.genRewrite.declaredat = [[
    // Declared at #declaredat
]]

Rewrite.genRewrite:conditional [[
    if ($Condition) {
      state().during#aspectName++;
      $ASTNode result = rewriteRule$RewriteIndex();
      state().during#aspectName--;
      return result;
    }
]]

Rewrite.genRewrite:unconditional [[
    state().during#aspectName++;
    $ASTNode result = rewriteRule$RewriteIndex();
    state().during#aspectName--;
    return result;
]]

RewriteList.genRewrite [[
    if (getParent().getParent() instanceof #getParentName &&
        ((#getParentName)getParent().getParent()).#(getChildName)ListNoTransform() == getParent()$Condition) {

      state().during#aspectName++;
      $List list = ($List) getParent();
      int i = list.getIndexOfChild(this);
      $List newList = rewrite#(getParentName)_#getChildName();
      // the first child is set by the normal rewrite loop
      //list.setChild(newList.getChildNoTransform(0), i);
      for(int j = 1; j < newList.getNumChildNoTransform(); j++)
        list.insertChild(newList.getChildNoTransform(j), ++i);

      state().during#aspectName--;
      return newList.getChildNoTransform(0);
    }
]]

Grammar.genRewriteOrderChecks = [[
protected int $ASTNode$$State.during$RewriteName = 0;
protected boolean $ASTNode.during$RewriteName() {
  if(state().during$RewriteName == 0) {
    return false;
  } else {
    state().pop();
    state().push($ASTNode$$State.REWRITE_INTERRUPT);
    return true;
  }
}
]]

Rewrite.javaDoc:internal [[
  /**
   * @declaredat #declaredat
   * @apilevel internal
   */]]

Grammar.genResetDuringCounters = [[
if(during$Name != 0) {
  System.out.println("Warning: resetting during$Name");
  during$Name = 0;
}
]]

ASTNode.rewrittenAttr = [[
$if(RewriteCircularNTA)
syn nta $ASTNode $ASTNode.rewritten(int i)
    circular [getChildNoTransform(i).treeCopyNoTransform()] =
  rewritten(i).rewriteTo();

$endif
]]
