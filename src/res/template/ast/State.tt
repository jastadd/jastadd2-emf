# Copyright (c) 2013, The JastAdd Team
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Lund University nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ASTNode$State = [[
/**
 * @apilevel internal
 */
public class $ASTNode$$State {
  $if(CacheCycle)
  /**
   * @apilevel internal
   */
  public boolean LAST_CYCLE = false;
  $endif

  /**
   * @apilevel internal
   */
  public boolean INTERMEDIATE_VALUE = false;

  /**
   * @apilevel internal
   */
  public boolean IN_CIRCLE = false;

  /**
   * @apilevel internal
   */
  public int CIRCLE_INDEX = 1;

  /**
   * @apilevel internal
   */
  public boolean CHANGE = false;

  /**
   * @apilevel internal
   */
  public boolean RESET_CYCLE = false;

  /**
   * @apilevel internal
   */
  static public class CircularValue {
    Object value;
    int visited = -1;
  }
  $if(ComponentCheck)
  public $DefaultSetType circularEvalSet = $CreateDefaultSet;

  public java.util.Stack circularEvalStack = new java.util.Stack();

  /**
   * @apilevel internal
   */
  static class CircularEvalEntry {
    $ASTNode node;
    String attrName;
    Object parameters;

    public CircularEvalEntry($ASTNode node, String attrName, Object parameters) {
      this.node = node;
      this.attrName = attrName;
      this.parameters = parameters;
    }

    public boolean equals(Object rhs) {
      CircularEvalEntry s = (CircularEvalEntry) rhs;
      if (parameters == null && s.parameters == null)
        return node == s.node && attrName.equals(s.attrName);
      else if (parameters != null && s.parameters != null)
        return node == s.node && attrName.equals(s.attrName) && parameters.equals(s.parameters);
      else
        return false;
    }

    public int hashCode() {
      return node.hashCode();
    }
  }

  public void addEvalEntry($ASTNode node, String attrName, Object parameters) {
    circularEvalSet.add(new CircularEvalEntry(node,attrName,parameters));
  }

  public boolean containsEvalEntry($ASTNode node, String attrName, Object parameters) {
    return circularEvalSet.contains(new CircularEvalEntry(node,attrName,parameters));
  }

  /**
   * @apilevel internal
   */
  static class CircularStackEntry {
    $DefaultSetType circularEvalSet;
    boolean changeValue;

    public CircularStackEntry($DefaultSetType set, boolean change) {
      circularEvalSet = set;
      changeValue = change;
    }
  }

  public void pushEvalStack() {
    circularEvalStack.push(new CircularStackEntry(circularEvalSet, CHANGE));
    circularEvalSet = $CreateDefaultSet;
    CHANGE = false;
  }

  public void popEvalStack() {
    CircularStackEntry c = (CircularStackEntry) circularEvalStack.pop();
    circularEvalSet = c.circularEvalSet;
    CHANGE = c.changeValue;
  }
  $endif
$if(RewriteEnabled)
  $if(HasRewriteLimit)
  /**
   * @apilevel internal
   */
  public $DefaultMapType debugRewrite = $CreateDefaultMap;
  $endif
  /**
   * @apilevel internal
   */
  public static final int REWRITE_CHANGE = 1;

  /**
   * @apilevel internal
   */
  public static final int REWRITE_NOCHANGE = 2;

  /**
   * @apilevel internal
   */
  public static final int REWRITE_INTERRUPT = 3;

  public int boundariesCrossed = 0;

  // state code
  private int[] stack;

  private int pos;

  public $ASTNode$$State() {
    stack = new int[64];
    pos = 0;
  }

  private void ensureSize(int size) {
    if(size < stack.length)
      return;
    int[] newStack = new int[stack.length * 2];
    System.arraycopy(stack, 0, newStack, 0, stack.length);
    stack = newStack;
  }

  public void push(int i) {
    ensureSize(pos+1);
    stack[pos++] = i;
  }

  public int pop() {
    return stack[--pos];
  }

  public int peek() {
    return stack[pos-1];
  }
  $if(StagedRewrites)
  public int rewritePhase = 1;
  $endif
$endif
  $include(State.incHook)

$if(TracingEnabled)
  private Trace trace = null;
  public Trace trace() {
    if (trace == null) {
      trace = new Trace();
    }
    return trace;
  }
  $include(TraceClass)
$endif
}
]]

ASTNode$State.reset = [[
  /**
   * @apilevel internal
   */
  public void $ASTNode$$State.reset() {
    $SynchBegin
    IN_CIRCLE = false;
    CIRCLE_INDEX = 1;
    CHANGE = false;
  $if(CacheCycle)
    LAST_CYCLE = false;
  $endif
  $if(ComponentCheck)
    circularEvalSet = $CreateDefaultSet;
    circularEvalStack = new java.util.Stack();
  $endif
$if(RewriteEnabled)
    boundariesCrossed = 0;
    #genResetDuringCounters
  $if(HasRewriteLimit)
    debugRewrite = $CreateDefaultMap;
  $endif
  $if(StagedRewrites)
    rewritePhase = 1;
  $endif
$endif
    $SynchEnd
  }
]]
