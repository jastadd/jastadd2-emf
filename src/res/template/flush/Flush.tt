# Copyright (c) 2013, The JastAdd Team
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Lund University nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ASTNode.flushTreeCacheMethod = [[
$if(FlushEnabled)
$if(#isASTNodeDecl)
  /**
    * @apilevel low-level
    */
  public void $ASTNode.flushTreeCache() {
    $SynchBegin
    flushCache();
    if (children == null) {
      return;
    }
    for (int i = 0; i < children.length; i++) {
      $if(RewriteEnabled)
      if (children[i] != null && (($ASTNode)children[i]).is$$Final) {
        (($ASTNode)children[i]).flushTreeCache();
      }
      $else
      if (children[i] != null) {
        (($ASTNode)children[i]).flushTreeCache();
      }
      $endif
    }
    $SynchEnd
  }
$endif
$endif
]]

ASTDecl.flushCacheMethod = [[
$if(FlushEnabled)
$if(#isASTNodeDecl)
  /**
    * @apilevel low-level
    */
  public void #name.flushCache() {
    $SynchBegin
    $if(FlushRewrite)
    flushRewriteCache();
    $endif
    flushAttrAndCollectionCache();
    $SynchEnd
  }
$endif
$endif
]]


ASTDecl.flushAttrAndCollectionCacheMethod = [[
$if(FlushEnabled)
$if(#isASTNodeDecl)
  /**
    * @apilevel internal
    */
  public void #name.flushAttrAndCollectionCache() {
    $SynchBegin
    $if(FlushAttr)
    flushAttrCache();
    $endif
    $if(FlushColl)
    flushCollectionCache();
    $endif
    $SynchEnd
  }
$endif
$endif
]]

ASTDecl.flushAttrCacheMethod = [[
$if(FlushAttr)
  /**
    * @apilevel internal
    */
  public void #name.flushAttrCache() {
    $if(!#isASTNodeDecl)
    super.flushAttrCache();
    $endif
    $FlushAttrCacheBody
  }
$endif
]]

ASTDecl.flushCollectionCacheMethod = [[
$if(FlushColl)
  /**
    * @apilevel internal
    */
  public void #name.flushCollectionCache() {
    $if(!#isASTNodeDecl)
    super.flushCollectionCache();
    $endif
    $FlushCollectionCache
  }
$endif
]]

# TODO: Figure out if this can be removed
TokenComponent.flushNTACache = [[
$if(FlushAttr)
if (((Object)token$(TypeSign)_$Id) instanceof $ASTNode) {
  token$(TypeSign)_$Id = null;
}
$endif
]]

AttrDecl.resetMethod = [[
$if(FlushAttr)
  /**
   * @apilevel internal
   */
  private void #(signature)_reset() {
    $include(AttrDecl.traceHookFlushAttr)
    $include(AttrDecl.resetAttrCache)
    $include(AttrDecl.resetAttrVisit)
  }
$endif
]]

AttrDecl.resetAttrCache = [[
$if(#isCircular)
 $if(#isParameterized)
  $if(LazyMaps)
#(signature)_values = null;
  $else
#(signature)_values = $CreateDefaultMap;
  $endif
  $if(#getNTA)
#(signature)_list = null;
  $endif
 $else
  $if(#isPrimitive)
#(signature)_computed = false;
#(signature)_initialized = false;
  $else
#(signature)_computed = false;
#(signature)_initialized = false;
#(signature)_value = null;
  $endif
 $endif
$else
 $if(#isLazy)
  $if(#isParameterized)
   $if(LazyMaps)
#(signature)_values = null;
   $else
#(signature)_values = $CreateDefaultMap;
   $endif
   $if(#getNTA)
#(signature)_list = null;
   $endif
  $else
   $if(#isPrimitive)
#(signature)_computed = false;
   $else
#(signature)_computed = false;
#(signature)_value = null;
   $endif 
  $endif
 $endif
$endif
]]

AttrDecl.resetAttrVisit = [[
$if(#isCircular)
 $if(!#isParameterized)
#(signature)_visited = -1;
 $endif
$else
 $if(VisitCheckEnabled)
  $if(RewriteEnabled)
   $if(#isParameterized)
    $if(LazyMaps)
#(signature)_visited = null;
    $else
#(signature)_visited = $CreateDefaultMap;
    $endif
   $else
#(signature)_visited = -1;
   $endif
  $else
   $if(#isParameterized)
    $if(LazyMaps)
#(signature)_visited = null;
    $else
#(signature)_visited = $CreateDefaultSet;
    $endif
   $else
#(signature)_visited = false;
   $endif
  $endif
 $endif
$endif
]]
