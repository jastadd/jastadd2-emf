# Copyright (c) 2013, The JastAdd Team
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Lund University nor the names of its
#       contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# TODO: Remove after incremental code has been refactored
# Removing maintenance of initial rewrite values
# These hooks are not included 
ASTNode.flushHookSetChildCopyState = [[
$if(FlushRewrite)
if (init_children != null) {
  $ASTNode d[] = new $ASTNode[i << 1];
  System.arraycopy(init_children, 0, d, 0, init_children.length);
  init_children = d;
}
if (children_computed != null) {
  boolean[] b = new boolean[i << 1];
  System.arraycopy(children_computed, 0, b, 0, children_computed.length);
  children_computed = b;
}
$endif
]]
ASTNode.flushHookSetChildInitState = [[
$if(FlushRewrite)
if (children_computed != null) {
  children_computed[i] = false;
}
$endif
]]
ASTNode.flushHookInsertChildUpdateState = [[
$if(FlushRewrite)
if (init_children != null) {
  $ASTNode d[] = new $ASTNode[init_children.length + 1];
  System.arraycopy(init_children, 0, d, 0, init_children.length);
  if (i < init_children.length) {
    System.arraycopy(init_children, i, d, i+1, init_children.length - i);
  }
  init_children = d;
}
if (children_computed != null) {
  boolean b[] = new boolean[children_computed.length + 1];
  System.arraycopy(children_computed, 0, b, 0, children_computed.length);
  if (i < children_computed.length) {
    System.arraycopy(children_computed, i, b, i+1, children_computed.length - i);
  }
  children_computed = b;
}
$endif
]]
ASTNode.flushHookRemoveChild = [[
$if(FlushRewrite)
if (init_children != null) {
  System.arraycopy(init_children, i+1, init_children, i, init_children.length-i-1);
}
if (children_computed != null) {
  System.arraycopy(children_computed, i+1, children_computed, i, children_computed.length-i-1);
}
$endif
]]


# Stores initial value for outermost rewrites on entry to evaluation case 1
ASTNode.flushHookRewriteEnterCircle = [[
$if(FlushRewrite)
if (!node.getChild_hasEnclosingInitValue()) {
  if(this.init_children == null) {
    this.init_children = new $ASTNode[this.children.length];
    this.children_computed = new boolean[this.children.length];
  }
  this.init_children[i] = node.treeCopyNoTransform();
  this.children_computed[i] = true;
  $include(ASTNode.traceHookFlushRewriteInit)
}
$endif
]]
